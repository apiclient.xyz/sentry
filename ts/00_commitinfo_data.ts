/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@mojoio/sentry',
  version: '2.0.1',
  description: 'an unofficial abstraction package for sentry.io'
}
